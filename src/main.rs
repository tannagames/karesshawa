/* Copyright (C) 2020 TannaGames
 * This program is licenced under the GNU-GPL v3.0.
 * Notice can be found in the LICENSE file.
 */

mod structs;
mod crystal;
mod crystals;
mod crystal_collection;
mod progress_bar;
use structs::{Player};
use crystals::{Goratanna, Hikaritanna};

fn main() {
  println!("¡세칻 따케!");
  let mut player1 = Player::new(String::from("MindBlower"), 8);
  println!("HERE COMES A NEW CHALLENGER\n{:#?}", &player1);
  player1.gora.push(Goratanna::new(100_000, 10));
  println!("\x1b[1mplayer1\x1b[0m has gained a goratanna!\n{:#?}", &player1);
  player1.hikari.push(Hikaritanna::new(100_000, 300_000, 10_000, 2_500));
  player1.hikari.push(Hikaritanna::new(50_000, 60_000, 6_000, 1_500));
  println!("\x1b[1mplayer1\x1b[0m has gained two hikaritanna!\n{:#?}", &player1);
  player1.hikari.damage(35_456);
  println!("\x1b[1mOOF! player1\x1b[0m has taken a huge hikari drain!\n{:#?}", &player1);
  // Simulating one second of hikari charging
  for _ in 0..60 {
    player1.charge_hikari(16);
    println!("\x1b[1mP1:\x1b[0m {:?}", player1.hikari.collection[1]);
  }
  player1.hikari.damage(40_000);
  println!("\x1b[1mOOF! player1\x1b[0m has taken ANOTHER huge hikari drain!\n{:#?}", &player1);
  
  std::process::exit(0_0)
}
