use crate::crystal::Crystal;

#[derive(Debug)]
pub struct CrystalCollection<T>
where T: Crystal {
  pub collection: Vec<T>,
  pub icurrent: usize,
}

#[allow(dead_code)]
impl<T> CrystalCollection<T>
where T: Crystal {
  pub fn new() -> CrystalCollection<T> {
    let collection = Vec::new();
    let mut icurrent = collection.len();
    if icurrent > 0 { icurrent -= 1; }
    CrystalCollection {
      collection,
      icurrent,
    }
  }
  
  /// Adds a crystal to the collection.
  /// # Panics
  /// Panics if the new capacity exceeds `isize::MAX` bytes.
  pub fn push(&mut self, crystal: T) {
    if self.collection.len() > 0 {
      self.icurrent += 1;
    }
    self.collection.push(crystal);
  }
  
  /// Removes the last crystal of the collection and
  /// returns it, or `None` if the collection is empty.
  pub fn pop(&mut self) -> Option<T> {
    if self.icurrent > 0 {
      self.icurrent -= 1;
    }
    self.collection.pop()
  }
  
  /// Returns the current crystal of the collection.
  pub fn current(&mut self) -> Option<&mut T> {
    if self.icurrent < self.collection.len() {
      Some(&mut self.collection[self.icurrent])
    } else {
      None
    }
  }
  
  /// Recovers `e` units of energy from the crystals in sequence,
  /// and returns the remaining units if it exceeds the maximum
  /// capacity of all the crystals.
  pub fn heal(&mut self, mut e: u64) -> u64 {
    let length = self.collection.len();
    while self.icurrent < length && e > 0 {
      let ocurrent = self.current();
      let current;
      match ocurrent {
        Some(c) => { current = c; }
        None => { return e; }
      }
      e = current.recover(e);
      self.icurrent += 1;
    }
    if self.icurrent == length {
      self.icurrent = length - 1;
    }
    e
  }
  
  /// Deals `e` units of damage to the crystals in sequence,
  /// and returns the remaining units if it exceeds the
  /// current energy of the crystal.
  pub fn damage(&mut self, mut e: u64) -> u64 {
    while e > 0 {
      let ocurrent = self.current();
      let current;
      match ocurrent {
        Some(c) => { current = c; }
        None => { return e; }
      }
      e = current.damage(e);
      if self.icurrent > 0 { self.icurrent -= 1; }
      else { break; }
    }
    e
  }
}
