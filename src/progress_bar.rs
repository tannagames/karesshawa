use std::fmt;

pub struct ProgressBar {
  pub i: u64,
  pub max: u64,
  pub width: usize,
  pub brackets: (char, char),
  ansi_color: String,
  pub text: (Option<String>, Option<String>),
}

impl ProgressBar {
  pub fn new(max: u64, width: usize, brackets: (char, char), color: u32, text: (Option<String>, Option<String>)) -> ProgressBar {
    let r = color >> 16;
    let g = (color >> 8) & 0xff;
    let b = color & 0xff;

    ProgressBar {
      i: 0,
      max,
      width,
      brackets,
      ansi_color: format!("\x1b[38;2;{};{};{}m", r, g, b),
      text,
    }
  }
}

const BLOCKS: [char; 9] = [' ', '▏', '▎', '▍', '▌', '▋', '▊', '▉', '█'];
impl fmt::Debug for ProgressBar {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    let (bleft, bright) = self.brackets;
    let (tleft, tright) = &self.text;

    let p = (self.i as f64) / (self.max as f64);
    let full = ((self.width as f64) * p).floor() as usize;
    let tiny = (((self.width as f64) * p - (full as f64)) * 8.).floor() as usize;
    
    let mut actual_bar: String = vec![BLOCKS[8]; full].iter().collect();
    if tiny > 0 { actual_bar.push(BLOCKS[tiny]); }
    if p < 1. {
      let mut nrepeat = self.width - full;
      if tiny > 0 { nrepeat -= 1; }
      actual_bar.push_str(&vec![BLOCKS[0]; nrepeat].iter().collect::<String>());
    }
    
    match tleft {
      Some(prefix) => {
        let _ = f.write_str(&format!("{} ", prefix));
      }
      None => ()
    }
    let r = f.write_str(
      &format!(
        "{}{}{}{}{} ({}/{})",
        bleft,
        self.ansi_color,
        actual_bar,
        "\x1b[39m",
        bright,
        self.i, self.max,
      )
    );
    match tright {
      Some(suffix) => {
        return f.write_str(&format!(" {}", suffix));
      }
      None => { return r; }
    };
  }
}