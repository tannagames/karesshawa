/* Copyright (C) 2020 TannaGames
 * This program is licenced under the GNU-GPL v3.0.
 * Notice can be found in the LICENSE file.
 */

use std::fmt;
use crate::crystal::Crystal;
use crate::progress_bar::ProgressBar;

pub struct Goratanna {
  pub capacity: u64,
  pub gora: u64,
  pub resistance: u64,
  pub hits: u64,
}

impl Goratanna {
  pub fn new(capacity: u64, resistance: u64) -> Goratanna {
    Goratanna {
      capacity,
      gora: capacity,
      resistance,
      hits: 0,
    }
  }
}

impl Crystal for Goratanna {
  fn capacity(&self) -> u64 { self.capacity }
  fn energy(&self) -> u64 { self.gora }
  fn set_energy(&mut self, e: u64) { self.gora = e; }
  fn recover(&mut self, e: u64) -> u64 { self.default_recover(e) }
  fn damage(&mut self, e: u64) -> u64 {
    self.hits += 1;
    self.default_damage(e)
  }
}

impl fmt::Debug for Goratanna {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    let mut gorabar = ProgressBar::new(
      self.capacity, 20,
      ('║', '║'),
      0xff1616,
      (
        Some(String::from("Goratanna")),
        Some(format!("| H/R: {}%", self.hits * 100 / self.resistance))
      )
    );
    gorabar.i = self.gora;
    gorabar.fmt(f)
  }
}

#[derive(Debug)]
#[allow(dead_code)]
pub enum HtStatus {
  Fine,
  Dented,
}

pub struct Hikaritanna {
  /// Amount of manimum Hikari that the Hikaritanna can contain.
  pub capacity: u64,
  /// Amount of Hikari contained in the Hikaritanna.
  pub hikari: u64,
  /// The time it takes in milliseconds to fully get its hikari back.
  pub fullcharge: u64,
  /// The time it takes in milliseconds to get half its hikari back.
  pub halfcharge: u64,
  /// How much damage the Hikaritanna can take without full recovery or full drain before getting dented.
  pub solidity: u64,
  /// How much damage the Hikaritanna is sustaining.
  pub vulnerabilities: u64,
  /// Whether the Hikaritanna is fine or dented.
  pub status: HtStatus,
}

impl Hikaritanna {
  pub fn new(capacity: u64, solidity: u64, fullcharge: u64, halfcharge: u64) -> Hikaritanna {
    Hikaritanna {
      capacity,
      hikari: capacity,
      fullcharge,
      halfcharge,
      solidity,
      vulnerabilities: 0,
      status: HtStatus::Fine,
    }
  }
}

impl fmt::Debug for Hikaritanna {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    let mut hikaribar = ProgressBar::new(
      self.capacity, 20,
      ('║', '║'),
      0x16ff32,
      (
        Some(String::from("Hikaritanna")),
        Some(format!("| V/S: {}% ({:?})", self.vulnerabilities * 100 / self.solidity, self.status)),
      )
    );
    hikaribar.i = self.hikari;
    hikaribar.fmt(f)
  }
}

impl Crystal for Hikaritanna {
  fn capacity(&self) -> u64 { self.capacity }
  fn energy(&self) -> u64 { self.hikari }
  fn set_energy(&mut self, e: u64) { self.hikari = e; }
  fn recover(&mut self, e: u64) -> u64 {
    match self.status {
      HtStatus::Dented => e,
      _ => {
        // Reset the vulnerabilities counter if the crystal is fully recovered
        let r = self.default_recover(e);
        if self.hikari == self.capacity { self.vulnerabilities = 0; }
        r
      },
    }
  }
  fn damage(&mut self, e: u64) -> u64 {
    let r = match self.status {
      HtStatus::Dented => e,
      _ => {
        // Reset the vulnerabilities counter if the crystal is fully drained
        let r = self.default_damage(e);
        if self.hikari == 0 { self.vulnerabilities = 0; }
        r
      },
    };
    self.vulnerabilities += e;
    if self.vulnerabilities >= self.solidity {
      self.vulnerabilities = self.solidity;
      self.status = HtStatus::Dented;
    }
    r
  }
}
