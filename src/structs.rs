/* Copyright (C) 2020 TannaGames
 * This program is licenced under the GNU-GPL v3.0.
 * Notice can be found in the LICENSE file.
 */

use crate::crystal_collection::CrystalCollection;
use crate::crystals::{Goratanna, Hikaritanna};

#[derive(Debug)]
pub struct Player {
  pub name: String,
  pub dexterity: u8,
  pub gora: CrystalCollection<Goratanna>,
  pub hikari: CrystalCollection<Hikaritanna>,
}

#[allow(dead_code)]
impl Player {
  pub fn new(name: String, dexterity: u8) -> Player {
    Player {
      name,
      dexterity,
      gora: CrystalCollection::new(),
      hikari: CrystalCollection::new(),
    }
  }

  pub fn charge_hikari(&mut self, ms: u64) {
    let hikari = &mut self.hikari;

    let ocurrent = hikari.current();
    match ocurrent {
      Some(current) => {
        let c_hikari = current.hikari;
        let c_capacity = current.capacity;
        let c_fullcharge = current.fullcharge;
        let c_halfcharge = current.halfcharge;
        
        if c_hikari < c_capacity / 2 {
          hikari.heal(c_capacity * ms / (2 * c_halfcharge));
        } else {
          hikari.heal(c_capacity * ms / (2 * (c_fullcharge - c_halfcharge)));
        }
      }
      None => ()
    }
  }
  
  pub fn update(&mut self, ms: u64) {
    self.charge_hikari(ms);
  }
}
