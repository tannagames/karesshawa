pub trait Crystal {
  fn capacity(&self) -> u64;
  fn energy(&self) -> u64;
  fn set_energy(&mut self, e: u64);
  /// Recovers `e` units of energy, and returns the remaining
  /// units if it exceeds the capacity of the crystal.
  fn recover(&mut self, e: u64) -> u64;
  /// Deals `e` units of damage, and returns the remaining
  /// units if it exceeds the current energy of the crystal.
  fn damage(&mut self, e: u64) -> u64;
  
  /// Default recover function.
  fn default_recover(&mut self, e: u64) -> u64 {
    let energy = self.energy();
    let capacity = self.capacity();
    let mut total = energy + e;
    let mut remaining = 0;
    if total > capacity {
      remaining = total - capacity;
      total = capacity;
    }
    self.set_energy(total);
    remaining
  }
  
  /// Default damage function.
  fn default_damage(&mut self, e: u64) -> u64 {
    let energy = self.energy();
    let mut total = 0;
    let mut remaining = 0;
    if energy < e { remaining = e - energy; }
    else { total = energy - e; }
    self.set_energy(total);
    remaining
  }
}
